package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	public List<Report> getReports(String start, String end) throws ParseException {

		Date since = null;
		Date until = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");

		if (start == null || start.equals("")) {
			since = format.parse("2020/12/01 00:00:00");
		} else {
			start += " 00:00:00";
			since = format.parse(start.replace("-", "/"));
		}

		if (end == null || end.equals("")) {
			until = new Date();
		} else {
			end += " 23:59:59";
			until = format.parse(end.replace("-", "/"));
		}

		List<Report> reports = reportRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(since, until);

		return reports;
	}

	public Report getReport(Integer id) {

		return reportRepository.getOne(id);
	}

	public void saveReport(Report report) {

		reportRepository.save(report);
	}

	public void deleteReport(Integer id) {

		reportRepository.deleteById(id);
	}
}
