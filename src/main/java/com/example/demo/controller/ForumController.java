package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Period;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	@GetMapping
	public ModelAndView top(@ModelAttribute("date") Period period) throws ParseException {

		ModelAndView mav = new ModelAndView();
		Comment newComment = new Comment();
		List<Report> reports = reportService.getReports(period.getStart(), period.getEnd());
		List<Comment> comments = commentService.getComments();

		mav.addObject("period", period);
		mav.addObject("newComment", newComment);
		mav.addObject("reports", reports);
		mav.addObject("comments", comments);
		mav.setViewName("top");
		return mav;
	}

	@GetMapping("/new")
	public ModelAndView newContent() {

		ModelAndView mav = new ModelAndView();
		Report report = new Report();

		mav.addObject("report", report);
		mav.setViewName("new");
		return mav;
	}

	@GetMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView();
		Report report = reportService.getReport(id);

		mav.addObject("report", report);
		mav.setViewName("update");
		return mav;
	}

	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("report") Report report) {

		report.setCreatedDate(new Date());
		report.setUpdatedDate(new Date());
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	@PostMapping("/addComment/{reportId}")
	public ModelAndView addComment(@ModelAttribute("newComment") Comment newComment, @PathVariable int reportId) {

		Report report = reportService.getReport(newComment.getReportId());

		report.setUpdatedDate(new Date());
		newComment.setCreatedDate(new Date());
		newComment.setUpdatedDate(new Date());
		reportService.saveReport(report);
		commentService.saveComment(newComment);
		return new ModelAndView("redirect:/");
	}

	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@ModelAttribute("report") Report report, @PathVariable Integer id) {

		report.setCreatedDate(reportService.getReport(id).getCreatedDate());
		report.setUpdatedDate(new Date());
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	@PutMapping("/updateComment/{id}")
	public ModelAndView updateComment(@ModelAttribute("comment") Comment comment, @PathVariable Integer id) {

		comment.setCreatedDate(commentService.getComment(id).getCreatedDate());
		comment.setUpdatedDate(new Date());
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {

		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {

		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}
}
